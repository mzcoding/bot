<?php

namespace App\Commands;


use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class WalletCommand extends Command
{
    protected $name = "wallet";

    protected $description = "Место для хранения монет";

    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $reply = "*💰Кошелек*.\n\nПереход в кошелек. ⁮   ⁮ ⁮ ⁮  ⁮   ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮   ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ";

        $this->replyWithMessage([
            'text' => $reply,
            'parse_mode' => 'Markdown',
            'disable_web_page_preview' => true
        ]);
    }
}