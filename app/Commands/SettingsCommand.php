<?php

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

/**
 * Class SettingsCommand
 * @package App\Commands
 * @uses Выводит панель настроек
 * @todo Выводить $val, $login из БД
 */
class SettingsCommand extends Command
{

    protected $val = "RUB"; // @todo Выводить $val из БД
    protected $login = "faker laravel";// @todo Выводить $login из БД
    protected $back = false;
    protected $chat_id;
    protected $message_id;

    /**
     * @var string Command Name
     */
    protected $name = "settings";

    /**
     * @var string Command Description
     */
    protected $description = "Изменение настроек работы бота";

    protected $aliases = ['⚙️Настройки'];

    function __construct()
    {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }
    }

    public function __construct0()
    {
    }

    public function __construct3($telegram, $chat_id, $message_id)
    {
        $this->telegram = $telegram;
        $this->chat_id = $chat_id;
        $this->message_id = $message_id;
        $this->back = true;
    }

    public function handle()
    {
        $reply = "⚙️*Настройки*.\n\nЧто Вы хотите изменить?\n\n*Текущий логин*: $this->login\n*Текущая валюта*: $this->val ⁮   ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮   ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮   ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ";

        $inline_settings = json_encode([ 'inline_keyboard' =>
            [
                [
                    ["text" => "💶Изменить выбранную валюту", "callback_data" => "currency.settings"],
                ],
                [
                    ["text" => "📫Адрес BTC-кошелька", "callback_data" => "address"],
                ],
                [
                    ["text" => "📊Курс BTC", "callback_data" => "course"],
                ]
            ]
        ]);

        if($this->back)
        {
            $this->telegram->editMessageText([
                'chat_id'       => $this->chat_id,
                'message_id'    => $this->message_id,
                'text'          => $reply,
                'reply_markup'  => $inline_settings,
                'parse_mode'    => 'Markdown',
            ]);
        }
        else
        {
            $this->replyWithChatAction(['action' => Actions::TYPING]);

            $this->replyWithMessage([
                'text' => $reply,
                'reply_markup' => $inline_settings,
                'parse_mode' => 'Markdown',
                'disable_web_page_preview' => true
            ]);
        }
    }
}