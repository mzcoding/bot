<?php


namespace App\Commands;


use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class FaqCommand extends Command
{
    protected $name = "faq";

    protected $description = "Ответы на самые популярные вопросы";
    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $inline_faq = json_encode([ 'inline_keyboard' =>
            [
                [
                    ["text" => "💬Поддержка", "url" => "https://www.google.com/intl/ru_us/contact/"],
                ],
                [
                    ["text" => "📄Условия", "url" => "https://policies.google.com/terms?hl=ru&gl=US"],
                ]
            ]
        ]);

        $reply = "❓*FAQ*.\n\nБот может использоваться как место хранения Bitcoin, а также как площадка для торговли и обмена монет. 
В момент сделки бот удерживает активы сторон, предотвращая таким образом любое мошенничество\n
Для получения более детальной информации о работе бота, а тажкже для просмотра наиболее популярных вопросов, воспользуйтесь данной 
[ссылкой на FAQ](https://en.wikipedia.org/wiki/FAQ)";

        $this->replyWithMessage([
            'text' => $reply,
            'reply_markup' => $inline_faq,
            'parse_mode' => 'Markdown',
            'disable_web_page_preview' => true
        ]);

    }
}