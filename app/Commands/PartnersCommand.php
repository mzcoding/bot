<?php


namespace App\Commands;


use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

/**
 * Class PartnersCommand
 * @package App\Commands
 * @uses Партнерская программа
 * @todo Выписывать статистику из БД - сколько людей пригласил пользователь, сколько переводов без комиссий у него осталось
 */
class PartnersCommand extends Command
{
    protected $name = "partners";
    protected $inviteNumb = "7"; //Кол-во приглашений пользователя @todo Брать статистику из БД
    protected $freeTrade = "19"; //Кол-во оставшихся бесплатных переводов @todo Брать статистику из БД
    protected $description = "Условия партнерской программы";
    protected $chat_id; //ID пользователя

    public function __construct()
    {
        $this->chat_id = 0;
    }

    public function handle()
    {
        $chat_id = $this->getUpdate()->getChat()->getId();
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $reply = "🎁*Партнерская программа*.\n\nПригласите нового пользователя и получите\n *3* обмена *без комиссий*!💰
\nКоличество приглашений не ограничено!\n \nИспользуйте данную ссылку для приглашение пользователей:⁮  ⁮   ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮⁮  ⁮   ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮⁮  ⁮   ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮  ⁮ ⁮ ⁮ ⁮ ⁮ ⁮ ⁮\n
[telegram.me/BillEx_bot?start=".$chat_id."]\n
💌*Кол-во приглашений*: ".$this->inviteNumb."\n💸*Кол-во бесплатных переводов*: ".$this->freeTrade;

        $this->replyWithMessage([
            'text' => $reply,
            'parse_mode' => 'Markdown',
            'disable_web_page_preview' => true
        ]);
    }
}