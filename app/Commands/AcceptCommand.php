<?php

namespace App\Commands;


use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class AcceptCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "accept";

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $common_main = json_encode([ "keyboard" =>
            [["💰Кошелек", "⚖️Обмен Bitcoin"], ["❓FAQ", "⚙️Настройки"], ["🎁Партнерская программа"]],
            'resize_keyboard'   => true,
            'one_time_keyboard' => false,
        ]);

        $inline_main = json_encode([ 'inline_keyboard' =>
            [
                [
                    ['text'=>"➕Купить BTC", 'callback_data'=>'buy.trading'], ['text'=>"➖Продать BTC", 'callback_data'=>'sell.trading']
                ],
                [
                    ['text'=>"💶Изм. валюту", 'callback_data'=>'currency.trading'], ['text'=>"🗂Мои объявления", 'callback_data'=> 'ads.trading']
                ],
            ]
        ]);

        $reply = "Приветствую Вас.\nЭто быстрый и безопасный кошелек, а также сервис для обмена Bitcoin";
        $this->replyWithMessage([
            'text' => $reply,
            'reply_markup' => $common_main,
            'parse_mode' => 'Markdown',
            'disable_web_page_preview' => true
        ]);
        $reply = "Если у Вас есть вопросы, перейдите по [ссылке](https://www.google.ru/) ⁮";
        $this->replyWithMessage([
            'text' => $reply,
            'reply_markup' => $inline_main,
            'parse_mode' => 'Markdown',
            'disable_web_page_preview' => true
        ]);

    }
}