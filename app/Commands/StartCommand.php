<?php

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "Команда для начала работы с ботом";

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);


        $reply = "Пожалуйста подтвердите, что Вы ознакомились и согласны с [условиями предоставления услуг](https://policies.google.com/terms?hl=ru&gl=US).";

        $common_start = json_encode(['keyboard' =>
            [["✅ Согласен"]],
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
        ]);

        $this->replyWithMessage([
            'text' => $reply,
            'reply_markup' => $common_start,
            'parse_mode' => 'Markdown',
            'disable_web_page_preview' => true
        ]);
    }

}