<?php


namespace App\Commands;


use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;


    /**
     * Class TradingCommand
     * @package App\Commands
     * @uses Обмен Биткоин
     * @todo Выводить ткущий курс биткоина в зависимости от вебранной в настройках площадки $course
     */
class TradingCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "trading";

    protected $back = false;
    protected $chat_id;
    protected $message_id;

    protected $currency = "RUB"; //Выбранная валюта @todo получать из БД
    protected $course = "Localbitcoins"; //Выбранная биржа для отображения курса @todo получать из БД
    protected $price = "500000"; // @todo Выводить примерный курс
    protected $balance = "0.123"; //Баланс в кошельке @todo получать из БД
    /**
     * @var string Command Description
     */
    protected $description = "Площадка для торговли монетами";

    function __construct()
    {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }
    }

    public function __construct0()
    {

    }

    public function __construct3($telegram, $chat_id, $message_id)
    {
        $this->telegram = $telegram;
        $this->chat_id = $chat_id;
        $this->message_id = $message_id;
        $this->back = true;
    }

    public function handle()
    {
        $reply = "⚖️*Покупка/Продажа* BTC\n\nЗдесь можно совершать сделки между людьми, а бот будет выступать как гарант-посредник.\n\nБиржевой курс($this->course): *".$this->price."* ".$this->currency.".\nБаланс: *".$this->balance."* BTC";

        $inline_trade = json_encode([ 'inline_keyboard' =>
            [
                [
                    ['text'=>"➕Купить BTC", 'callback_data'=>'buy'], ['text'=>"➖Продать BTC", 'callback_data'=>'sell']
                ],
                [
                    ['text'=>"💶Изм. валюту", 'callback_data'=>'currency.trading'], ['text'=>"🗂Мои объявления", 'callback_data'=> 'ads']
                ],
            ]
        ]);

        if($this->back)
        {
            $this->telegram->editMessageText([
                'chat_id'       => $this->chat_id,
                'message_id'    => $this->message_id,
                'text'          => $reply,
                'reply_markup'  => $inline_trade,
                'parse_mode'    => 'Markdown',
            ]);

        }
        else
        {
            $this->replyWithChatAction(['action' => Actions::TYPING]);

            $this->replyWithMessage([
                'text' => $reply,
                'reply_markup' => $inline_trade,
                'parse_mode' => 'Markdown',
                'disable_web_page_preview' => true
            ]);
        }
    }
}