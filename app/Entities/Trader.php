<?php

class Trader extends BaseModel
{
    protected $table = "traders";

    /**
     * @param array $data
     * @return int
     * @throws \Exception
     */
    public function addTrader(array $data)
    {
        $login_bot = $data['login_bot'];
        $object = $this->getTrader($login_bot);
        if($object->id) {
            return $this->getItem($object->id);
        }

        $trader = $this->insertData($data);
        if(!$trader) {
            throw new \Exception("Trader add error");
        }

        return $trader;
    }

    /**
     * @return array
     */
    public function getTraders()
    {
        $traders = $this->getData();
        return $traders;
    }

    /**
     * @param $login_bot
     * @return mixed
     */
    public function getTrader($login_bot)
    {
        $query = $this->db->query("SELECT * from {$this->table} 
                                            WHERE login_bot = '".$login_bot."'");
        return $query->fetchObject();

    }
}