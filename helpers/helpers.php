<?php
/**
 * Created by MessageBotScript.
 * Version: 1.0
 * User: Mzcoding <mzcoding@gmail.com>
 */

if(!function_exists('abort404')) {
    /**
     * 404 error generate
     */
    function abort404()
    {
        $config = include __DIR__ . '/../config/config.php';
        $lang = $config['language'] ?? 'english';

        if (file_exists(__DIR__ . '/../resources/views/' . $lang . '/404.phtml')) {
            echo include __DIR__ . '/../resources/views/' . $lang . '/404.phtml';
            die;
        }

        echo "Error 404: Address not found";

    }
}
if(!function_exists('dd')) {
    function dd($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die;
    }
}