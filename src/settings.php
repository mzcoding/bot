<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
    ],
    'bot' => [
        'base_url'  => 'https://freelance.app',
        'language'  => 'russian',
        'token' =>   '671535614:AAFIj51w2QxsVubalNd9au1v3xhWZbUHD7Q',

        //'587544852:AAHP5351XdmUE_J1fgNkJ4YVAgLXkIMeCd4'
    ],
    'database' => [
        'host'     => 'localhost',
        'name'     => 'bot',
        'user'     => 'root',
        'password' => ''
    ],
];
