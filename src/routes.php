<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/telegram/getme', function(Request $request, Response $response, array $args)
use($app){

    $di = $app->getContainer();
    $telegram = $di->get('telegram')['telegram'];
    $response = $telegram->getMe();
    $botId = $response->getId();
    $update = $di->get('telegram')['update'];


});
