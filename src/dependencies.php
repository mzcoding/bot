<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};
//database
$container['db'] = function($c){
    $db = $c->get('settings')['db'];


    $host = $db['host'];
    $name = $db['name'];
    $user = $db['user'];
    $password = $db['password'];

    $dsn = "mysql:host=$host;dbname=$name";

    $dbh = new \PDO($dsn, $user, $password);
    if(!$dbh) {
        throw new \PDOException("Don't connected to $name database");
    }
    return $dbh;
};
//bot
$container['telegram'] = function($c) {
    $token = $c->get('bot')['token'];
    $telegram = new Telegram\Bot\Api($token);
    $registerCommands = function($telegram) {
        //register commands
        $telegram->addCommand([
            \App\Commands\StartCommand::class,
            \App\Commands\HelpCommand::class,
            \App\Commands\AcceptCommand::class,
            \App\Commands\FaqCommand::class,
            \App\Commands\PartnersCommand::class,
            \App\Commands\SettingsCommand::class,
            \App\Commands\TradingCommand::class,
        ]);
    };

    $registerCommands($telegram);
    $update = $telegram->commandsHandler(true);

    return ['telegram' => $telegram, 'update' => $update];
};